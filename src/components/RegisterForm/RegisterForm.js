import {
    Form,
    Input,
    Button,
    Modal,
} from 'antd';
import history from "../history/history";
import {gql, useMutation} from "@apollo/client";

import React, { useState } from 'react';

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 10,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const RegisterForm = () => {
    const [form] = Form.useForm();

    const REGISTER_USER = gql`
	  mutation signUpz($input: SignUpInput!) {
          signUp(input: $input) {
            _empty
          }
        }
	`;

    const [signUp, {data, loading, error}] = useMutation(REGISTER_USER, {
        onError: (err) => {
            Modal.error({
                title: 'Error during Registration',
                content: err.graphQLErrors.map(
                    (err, i) =>
                        <p key={'err' + i}>{err.message}</p>
                )
            })
        },
        onCompleted: (data) => {
            history.push('/Dashboard');
        }
    });

    const onFinish = (values) => {
        signUp({
            variables: {
                input: {
                    givenName: values.givenName,
                    familyName: values.familyName,
                    email: values.email,
                    password: values.password
                }
            }
        });
    };

    return (
        <>
            <div className="form-login">
                <Form {...formItemLayout}
                      form={form}
                      name="register"
                      className="login-form"
                      onFinish={onFinish}
                      scrollToFirstError
                >
                    <Form.Item
                        name="email"
                        label="E-mail"
                        rules={[
                            {
                                required: true,
                                type: 'email',
                                message: 'The input is not valid E-mail!',
                            },
                        ]}
                    >
                        <Input size="large"/>
                    </Form.Item>
                    <Form.Item
                        name="givenName"
                        label="First Name"
                        rules={[
                            {
                                required: true,
                                message: 'The input is not valid First Name!',
                            },
                        ]}
                    >
                        <Input size="large"/>
                    </Form.Item>

                    <Form.Item
                        name="familyName"
                        label="Last Name"
                        rules={[
                            {
                                message: 'The input is not valid Last Name!',
                                required: true,
                            },
                        ]}
                    >
                        <Input size="large"/>
                    </Form.Item>

                    <Form.Item
                        name="password"
                        label="Password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                        hasFeedback
                    >
                        <Input.Password size="large"/>
                    </Form.Item>

                    <Form.Item
                        name="confirm"
                        label="Confirm Password"
                        dependencies={['password']}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Please confirm your password!',
                            },
                            ({getFieldValue}) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve();
                                    }

                                    return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                },
                            }),
                        ]}
                    >
                        <Input.Password size="large"/>
                    </Form.Item>

                    <Form.Item {...tailFormItemLayout}>
                        <Button size="large" type="primary" htmlType="submit" className="register-form-button">
                            Register
                        </Button>
                        <Button size="large" onClick={() => history.push('/')}>Back</Button>
                    </Form.Item>
                </Form>
            </div>
        </>
    );
};

export default RegisterForm;

