import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";

import LoginForm from "../LoginForm/LoginForm";
import RegisterForm from "../RegisterForm/RegisterForm";
import Dashboard from "../Dashboard/Dashboard";
import history from "../history/history";

export default class Routes extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route path="/" exact component={LoginForm} />
                    <Route path="/Register" component={RegisterForm} />
                    <Route path="/Dashboard" component={Dashboard} />
                </Switch>
            </Router>
        )
    }
}