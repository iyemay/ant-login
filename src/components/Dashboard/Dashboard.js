import {Button} from "antd";
import history from "../history/history";

const Dashboard = (props) => {

    return (
        <div className="form-login">
            <p className="dashboard-text">Welcome!!!!! {props.name}</p>
            <Button onClick={() => history.push('/')} className="logout-button">Logout</Button>
        </div>
    );
}

export default Dashboard;