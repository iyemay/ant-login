import {Form, Input, Button} from 'antd';
import {UserOutlined, LockOutlined} from '@ant-design/icons';
import history from "../history/history";
import {gql, useMutation} from '@apollo/client';
import React from "react";

const LoginForm = () => {

    const GET_LOGIN_USER = gql`
	  mutation signIn($input: SignInInput!) {
		signIn(input: $input) {
		  user {
			  id
			}
		}
	  }
	`;

    const [signIn, {data, loading, error}] = useMutation(GET_LOGIN_USER);

    const onFinish = (values) => {
        signIn({
            variables: {
                input: {
                    ...values
                }
            }
        });
        history.push('/Dashboard');
    };

    return (
        <div className="form-login">
            <Form
                name="normal_login"
                className="login-form"
                onFinish={onFinish}
            >
                <Form.Item
                    name="email"
                    rules={[
                        {
                            type: 'email',
                            required: true,
                            message: 'Please input your Email!',
                        },
                    ]}
                >
                    <Input
                        size="large"
                        prefix={<UserOutlined
                            className="site-form-item-icon"/>}
                        placeholder="Email"/>
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                    ]}
                >
                    <Input
                        size="large"
                        prefix={<LockOutlined className="site-form-item-icon"/>}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary"
                            size="large"
                            htmlType="submit"
                            className="login-form-button"
                    >
                        Log in
                    </Button>
                    Or <Button size="large" onClick={() => history.push('/Register')}>register now!</Button>
                </Form.Item>
            </Form>
        </div>

    );
};

export default LoginForm;